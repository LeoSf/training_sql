/* Some comments

    ctrl + /    block commet 
    ctrl + j    open/close panel
    =, and, or, <>, is not null
    currdate()
    timestampdiff(year, start, end)
 */


show databases;

/* basic example to create a table  */
create table student(
    student_id int primary key,
    name varchar(20),
    major varchar(20)
);

/* to find out what databases currently exist on the server: */
SHOW DATABASES;
/* 
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
+--------------------+ */
/* The mysql database describes user access privileges */

/* 4.1 Creating and Selecting a Database */
CREATE DATABASE menagerie;

use menagerie
/* Database changed */

/* Alternatively, you can select the database on the command 
line when you invoke mysql. Just specify its name after any 
connection parameters that you might need to provide. For example:
    shell> mysql -h host -u user -p menagerie
Enter password: ******** */

/* You can see at any time which database is currently selected 
using:  */
SELECT DATABASE();

show tables;

create table pet(
    name varchar(20),
    owner varchar(20),
    species varchar(20),
    sex char(1),
    birth date,
    death date
);

/* To verify that the table was created as expected */
describe pet;

/* To load the text file pet.txt into the pet table.
data have to ve separated with tab and null can be represented with \N*/
/* path: /home/leo-server/files/pet.txt */  
load data local infile '/home/leo-server/files/pet.txt' into table pet;

/* checking the result */
select * from pet;

/* inserting data in one line */
insert into pet
values(
    'Puffball', 'Diane', 'hamster', 'f', '199-03-30', NULL
);

/* 3.4 Retrieving information from table */
select owner
from pet
where sex = 'f';

/* delete everything from the table */
delete from pet;
/* after this you can load everything from the file */

/* update a single field */
update pet set sex = 'f' where name = 'Slim';

/* selecting particular rows */
select * from pet where name = 'Bowser';

select * from pet where birth >= '1998-1-1';

select * from pet where birth >= '1998-1-1' and sex = 'f';


/* Selecting particular columns */
select name, birth from pet;

/* if some queries retrieves more than one result and some of them 
are duplicated, the list can be reduced with: */
select distinct owner from pet;


/* Sorting rows */
select name, birth from pet order by birth;

/* You can force a case-sensitive sort for a column by using BINARY like so: 
ORDER BY BINARY col_name. */

/* sorting with descending order */
select name, birth from pet order by birth desc;

/* You can sort on multiple columns, and you can sort different columns in 
different directions. For example, to sort by type of animal in ascending 
order, then by birth date within animal type in descending order (youngest 
animals first), use the following query: */

select name, species, birth from pet
order by species, birth desc;

/* Date calculations */
select name, birth, curdate(),
timestampdiff( year, birth, curdate()) as age 
from pet;
/* output:
+----------+------------+------------+------+
| name     | birth      | curdate()  | age  |
+----------+------------+------------+------+
| Fluffy   | 1993-02-04 | 2019-12-23 |   26 |
| Claws    | 1994-03-17 | 2019-12-23 |   25 |
| Buffy    | 1989-05-13 | 2019-12-23 |   30 |
| Fang     | 1990-08-27 | 2019-12-23 |   29 |
| Bowser   | 1979-08-31 | 2019-12-23 |   40 |
| Chirpy   | 1998-09-11 | 2019-12-23 |   21 |
| Whistler | 1997-12-09 | 2019-12-23 |   22 |
| Slim     | 1996-04-29 | 2019-12-23 |   23 |
+----------+------------+------------+------+
8 rows in set (0.000 sec) */

/* valid parameters:
    year, moth, day, minute, second */

/* query selecting animals that haven't die yet */
select name, birth, death, 
timestampdiff(year, birth, death) as age
from pet
where death is not null order by age desc;

/* selecting the month of the birthday */
select name, birth, month(birth) from pet;

/* check the upcoming birthdays when next month is may */
select name, birth 
from pet 
where month(birth) = 5;

/* a special case is if I want to determine the upcoming birthdays
and we are in december. Two posibles solutions arise:
    * with time interval
    * adding 1 to a date 
*/
select name, birth from pet
where month(birth) = month(date_add(curdate(), interval 1 month));

select name, birth from pet
where month(birth) = mod(month(curdate()), 12) +1;

/* checking */
select name, birth from pet order by month(birth) desc;


SELECT '2018-10-31' + INTERVAL 1 DAY;
/* 
+-------------------------------+
| '2018-10-31' + INTERVAL 1 DAY |
+-------------------------------+
| 2018-11-01                    |
+-------------------------------+ */

SELECT '2018-10-32' + INTERVAL 1 DAY;
/* 
+-------------------------------+
| '2018-10-32' + INTERVAL 1 DAY |
+-------------------------------+
| NULL                          |
+-------------------------------+ */

SHOW WARNINGS;
/* 
+---------+------+----------------------------------------+
| Level   | Code | Message                                |
+---------+------+----------------------------------------+
| Warning | 1292 | Incorrect datetime value: '2018-10-32' |
+---------+------+----------------------------------------+ */


/* Working with null values */
select 1 is null, 1 is not null;

select 1 = null, 1 <> null, 1 < null, 1 > null;

/*  IMPORTANT:
The result of any arithmetic comparison with NULL is also NULL
In MySQL, 0 or NULL means false and anything else means true. 
The default truth value from a boolean operation is 1. 
Two NULL values are regarded as equal in a GROUP BY.*/

/* NULL es solo null, 0 or '' are not NULL */


/* ---- Pattern Matching ---- */

/* To find names beginning with b: */
SELECT * FROM pet WHERE name LIKE 'b%';

/* To find names ending with fy: */
SELECT * FROM pet WHERE name LIKE '%fy';

/* To find names containing a w: */
SELECT * FROM pet WHERE name LIKE '%w%';

/* To find names containing exactly five characters, use five instances of the _ pattern character: */
SELECT * FROM pet WHERE name LIKE '_____';

/* -------------------------------------------------------------------------------  */
/* Using some regex expressions                                                     */

/* To find names beginning with b, use ^ to match the beginning of the name: */
SELECT * FROM pet WHERE name REGEXP '^b';

/* To force a REGEXP comparison to be case-sensitive, use the BINARY keyword to make 
one of the strings a binary string. 
This query matches only lowercase b at the beginning of a name: */
SELECT * FROM pet WHERE name REGEXP BINARY '^b';

/* To find names ending with fy, use $ to match the end of the name: */
SELECT * FROM pet WHERE name REGEXP 'fy$';

/* To find names containing a w, use this query: */
SELECT * FROM pet WHERE name REGEXP 'w';

/* To find names containing exactly five characters, use ^ and $ to match the beginning 
and end of the name, and five instances of . in between: */
SELECT * FROM pet WHERE name REGEXP '^.....$';

/* You could also write the previous query using the {n} (“repeat-n-times”) operator: */
SELECT * FROM pet WHERE name REGEXP '^.{5}$';

/* ----- Counting rows ----- */
select count(*) from pet;

/* count the amount of diferent species of each owner */
select owner, count(*)
from pet
group by especies;

/* some stadistics */
/*  number of animals per spcies */
select species, count(*) 
from pet
group by species
order by count(*) desc;

/* number of animals per sex */
select sex, count(*) 
from pet
group by(sex);


/* Number of animals per combination of species and sex */
select species, sex, count(*)
from pet
group by species, sex;

/* the same as adobe but only for cat and dogs */

select species, sex, count(*)
from pet
where species = 'cat' or species = 'dog'
group by species, sex;

/* discarding entries wih unknown sex */
select species, sex, count(*)
from pet
where sex is not null
group by species, sex;

/* 
If you name columns to select in addition to the COUNT() value, a GROUP BY clause should 
be present that names those same columns. Otherwise, the following occurs:
    • If the ONLY_FULL_GROUP_BY SQL mode is enabled, an error occurs:
    mysql> SET sql_mode = 'ONLY_FULL_GROUP_BY';
    Query OK, 0 rows affected (0.00 sec)
    
    mysql> SELECT owner, COUNT(*) FROM pet;
    ERROR 1140 (42000): Mixing of GROUP columns (MIN(),MAX(),COUNT()...)

    with no GROUP columns is illegal if there is no GROUP BY clause

    • If ONLY_FULL_GROUP_BY is not enabled, the query is processed by treating all rows 
    as a single group, but the value selected for each named column is nondeterministic. 
    The server is free to select the value from any row:
    
    mysql> SET sql_mode = '';
    Query OK, 0 rows affected (0.00 sec)
    
    mysql> SELECT owner, COUNT(*) FROM pet;
    +--------+----------+
    | owner  | COUNT(*) |
    +--------+----------+
    | Harold |     8    |
    +--------+----------+
    1 row in set (0.00 sec)
 */

/* USING MORE THAN ONE TABLE */

create table event(
    name varchar (20),
    date date,
    type varchar(15),
    remark varchar(255)
);

/* /home/leo-server/files/second_table.txt */
load data local infile '/home/leo-server/files/second_table.txt' into table event;

/* delete a tanle */
drop table event;
/* delete everything in a table */
delete from event;



/* excercise: we want to find out the ages at which each pet had its litters */
select pet.name, 
timestampdiff(year, pet.birth, event.date) as age,
remark
from pet inner join event
    on pet.name = event.name
where event.type = 'litter';

/* making n inner join with one table */
select p1.name, p1.sex, p2.name, p2.sex, p1.species
from pet as p1 inner join pet as p2
    on p1.species = p2.species
    and p1.sex = 'f' and p1.death is null
    and p2.sex = 'm' and p2.death is null;


/* -------------------------------------------------------------------------------  */
/* Chapter 5 Getting Information About Databases and Tables                         */
/* -------------------------------------------------------------------------------  */

/* list databases managed by the server */
show databases;

/* to find out wich database is currently selected */
select database();

/* to show tables */
show tables;

/* to show the structure of a table */
describe pet;
desc pet;

/* if the table has indexes */
show index from pet;


/* -------------------------------------------------------------------------------  */
/* Chapter 6 Using mysql in Batch Mode                                              */
/* -------------------------------------------------------------------------------  */


